# Copyright 2012, 2013 Johannes Nixdorf <mixi@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require pwmt freedesktop-desktop

export_exlib_phases src_compile src_install

SUMMARY="A plugin that adds PDF support to zathura by using the poppler rendering engine"

LICENCES="ZLIB"
SLOT="0"
MYOPTIONS="doc"

DEPENDENCIES="
    build:
        virtual/pkg-config
        doc? ( app-doc/doxygen )
    build+run:
        app-text/poppler[cairo][glib][>=0.18.0] [[ note = [ see pdf.c:13 ] ]]
        app-text/zathura[>=0.2.0]
        x11-libs/cairo
        x11-libs/girara

        !text-plugins/zathura-pdf-mupdf [[ description = [ They both provide colliding PDF plugins for zathura ]
                                           note = [ They collide in /usr/\${LIBDIR}/zathura/pdf.so ] ]]
"

BUGS_TO="mixi@exherbo.org"

zathura-pdf-poppler_src_compile() {
    emake \
        all $(optionv doc)
}

zathura-pdf-poppler_src_install() {
    default
    option doc && dodoc -r doc/html
}

